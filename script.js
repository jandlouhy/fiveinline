(function($) {
	
	var settings, clicked;
	
	var directions = ['N', 'NE', 'E', 'SE', 'S', 'SW', 'W', 'NW'];
	
	var currentPlayer = 0;
	
	$.FiveInLine = function(options) {
		settings = $.extend({}, defaults, options);
		initGameBoard();
		$("body").on("click", "span.empty", squareClick);
	};
	
	var defaults = {
		gameBoardId: "board",
		size: 10,
		inLine: 5
	};
	
	function initGameBoard() {
		currentPlayer = 0;
		
		clicked = [];
		
		var i, j, boardHtml = [];
		
		boardHtml.push("<div id='"+settings.gameBoardId+"'>");
		for (i = 0; i <= settings.size; i++) {
			boardHtml.push("<div class='row'>");
			for (j = 0; j <= settings.size; j++) {
				boardHtml.push("<span class='square empty' data-board-x='"+j+"' data-board-y='"+i+"'></span>");
			}
			boardHtml.push("</div>");
		}
		boardHtml.push("</div>");
		$("body").html(boardHtml.join(''));
		changePlayer();
	}
	
	function squareClick(event) {
		var target = $(event.target);
		
		var squareX = target.data("board-x"),
			 squareY = target.data("board-y");
		
		target.addClass("player"+currentPlayer);
		target.removeClass("empty");

		if (typeof clicked[squareX] === "undefined") {
			clicked[squareX] = [];
		}
		clicked[squareX][squareY] = currentPlayer;
		
		if(checkWin(squareX, squareY)) {
			alert("Hráč "+currentPlayer+" vyhrál!");
			initGameBoard();
		} else {
			changePlayer();
		}

		return this;
	};
	
	function checkWin(x, y) {
		var counter = [];
		for (direction in directions) {
			if (directions.hasOwnProperty(direction)) {
				counter[directions[direction]] = checkInDirection(directions[direction], { x: x, y: y}, 1);
			}
		}
		return checkCounter(counter);
	}
	
	function checkInDirection(direction, current, count) {
		if (count >= settings.inLine) {
			return count;
		}
		current = getClickedInDirection(direction, current);
		if (typeof clicked[current.x] !== "undefined"
				  && typeof clicked[current.x][current.y] !== "undefined"
				  && clicked[current.x][current.y] === currentPlayer) {
			return checkInDirection(direction, current, ++count);
		}
		return count;
	}
	
	function getClickedInDirection(direction, current) {
		var position = {};
		switch (direction) {
			case 'N': position = {x: current.x, y: current.y - 1}; break;
			case 'NE': position = {x: current.x + 1, y: current.y - 1}; break;
			case 'E': position = {x: current.x + 1, y: current.y}; break;
			case 'SE': position = {x: current.x + 1, y: current.y + 1}; break;
			case 'S': position = {x: current.x, y: current.y + 1}; break;
			case 'SW': position = {x: current.x - 1, y: current.y + 1}; break;
			case 'W': position = {x: current.x - 1, y: current.y}; break;
			case 'NW': position = {x: current.x - 1, y: current.y - 1}; break;
			default: console.error("Direction not found");
		}
		return position;
	}
	
	function checkCounter(counter) {
		var sum = false;
		sum |= (counter["N"] + counter["S"]) > settings.inLine;
		sum |= (counter["NE"] + counter["SW"]) > settings.inLine;
		sum |= (counter["E"] + counter["W"]) > settings.inLine;
		sum |= (counter["SE"] + counter["NW"]) > settings.inLine;
		return sum;
	}
	
	function changePlayer() {
		$("span.square").removeClass("hover-player1").removeClass("hover-player2");
		if (currentPlayer !== 1) {
			currentPlayer = 1;
			$("span.square").addClass("hover-player1");
		} else {
			currentPlayer = 2;
			$("span.square").addClass("hover-player2");
		}
	}
})(jQuery);